# Integrating Data and Knowledge to Support the Selection of Service Plant Species in Agroecology

_Warning: preliminary version. An English version of the KB will be provided._

## Description

This repository contains additional material to the paper "Integrating Data and Knowledge to Support the Selection of Service Plant Species in Agroecology" by Elie Najm, Marie-Laure Mugnier, Christian Gary, Jean-François Baget, Raphael Métral and Léo Garcia. 

## Content

- **TFS-diagrams**: Trait-Function-Service diagrams for three ecosystem services (nitrogen supply to the vine, water storage and supply to the vine, soil structuration);

- **species-ranking-literature.pdf**: ranking of species from scientific literature; used for the evaluation of the tool on a selected set of species;

- **Rules folder**: This folder provides all the rules using the French language for names (i.e., concepts, relations and variables). For now, only the domain ontology has been translated in English (see file domain-ontology-english.txt). Rules are specified using the datalog syntax, i.e.: 
  `head:-body.`

- **Facts folder**: This folder contains the expert facts for the nitrogen supply service (in French). Data facts will be provided subject to the agreement of the owners of data from which the facts have been built.  

## Acknowledgments

The authors are grateful to Gaëlle Damour, Eric Garnier, Elena Kazakou and Aurélie Métay for fruitful discussions and contributions to trait-function-service diagrams, as well as the providers of data: the TRY initiative (https://www.try-db.org/) and Sébastien Minette for  the second traits database. This work was supported by a governmental grant managed by the Agence Nationale de la Recherche (ANR) within the digital agriculture institute (https://www.hdigitag.fr/en/).